let alexaVerifier = require('alexa-verifier')
let express = require('express'),
    bodyParser = require('body-parser'),
    app = express();

var request = require('request');

function requestVerifier(req, res, next) {
    alexaVerifier(
        req.headers.signaturecertchainurl,
        req.headers.signature,
        req.rawBody,
        function verificationCallback(err) {
            if (err) {
                res.status(401).json({
                    message: 'Verification Failure',
                    error: err
                });
            } else {
                next();
            }
        }
    );
}

app.use(bodyParser.json({
    verify: function getRawBody(req, res, buf) {
        req.rawBody = buf.toString();
    }
}));

app.post('/forecast', requestVerifier, function (req, res) {
    console.log(req.body.request.type)
    console.log(req.body.request)
    if (req.body.session.user.userId != 'amzn1.ask.account.AGKD3BDNN6T6IMNC3ZFZZBM3IRJFQQQ6N2PBNWAEDMJJTLCOCKHRAPYWBOF74PNPBTBKZXAWR26AQHEFXJYOBKPGUOQ6CG6FQ373BXTHVSMHXNOLQU4B34UUIN6RMKL4OZXY5LXNH4BIGGWRKLN5QZNRBMG5SRIDYYRNC62ROE2E7BKQQB7P5WXH3LKJRJKDKJIE4U5HMUJJ7DY') {

        res.json({
            "version": "1.0",
            "response": {
                "shouldEndSession": true,
                "outputSpeech": {
                    "type": "SSML",
                    "ssml": "<speak>Du bist leider nicht berechtigt!</speak>"
                }
            }
        })
    }
    if (req.body.request.type === 'LaunchRequest') { /* ... */ } else if (req.body.request.type === 'SessionEndedRequest') { /* ... */ } else if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'rimo') {

        if (!req.body.request.intent.slots.Day ||
            !req.body.request.intent.slots.Day.value) {
            // Handle this error by producing a response like:
            // "Hmm, what day do you want to know the forecast for?"
        }
        let day = new Date(req.body.request.intent.slots.Day.value);
        console.log(req.body.request.intent.slots.Day)
            // Do your business logic to get weather data here!
            // Then send a JSON response...

        res.json({
            "version": "1.0",
            "response": {
                "shouldEndSession": true,
                "outputSpeech": {
                    "type": "SSML",
                    "ssml": "<speak>Finger in Po, Mexiko! Paris - Athen - auf wiedersehn! " + day + "</speak>"
                }
            }
        });
    };

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'user') {

        // Do your business logic to get weather data here!
        // Then send a JSON response...
console.log(req.body.request.intent.slots.ScoreOneMulti)
        console.log(req.body.request.intent.slots.ScoreOne)
console.log(req.body.request.intent.slots.ScoreTwoMulti)
        console.log(req.body.request.intent.slots.ScoreTwo)
console.log(req.body.request.intent.slots.ScoreThreeMulti)
        console.log(req.body.request.intent.slots.ScoreThree)
        request.post({
            url: 'https://rimo-saas.com/api/MIT_Powerlines_SM/SDAlexaRESTController',
            form: '{ "operation": "getUserCount" }'
        }, function (error, response, body) {
            console.log(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>Es sind " + body + " User aktiv!</speak>"
                    }
                }
            });
        });

        // Then send a JSON response...

    }


    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'sessions') {

        request.post({
            url: 'https://rimo-saas.com/api/MIT_Powerlines_SM/SDAlexaRESTController',
            form: '{ "operation": "getSessionCount" }'
        }, function (error, response, body) {
            console.log(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>Es sind " + body + " Sessions aktiv!</speak>"
                    }
                }
            });
        });

        // Then send a JSON response...

    }

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'debug') {

        request.post({
            url: 'https://rimo-saas.com/api/MIT_Powerlines_SM/SDAlexaRESTController',
            form: '{ "operation": "getDebugCount" }'
        }, function (error, response, body) {
            console.log(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>Es gibt " + body + " Debugs!</speak>"
                    }
                }
            });
        });

        // Then send a JSON response...

    }
});
app.listen(3000);
