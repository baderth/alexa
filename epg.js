let alexaVerifier = require('alexa-verifier')
let express = require('express'),
    bodyParser = require('body-parser'),
    app = express();

var request = require('request');
var moment = require('moment');

var zapTo = function (station) {
    if (station == 'o. r. f. eins') {
        station = 'ORF_eins_HD'
    };
    if (station == 'o. r. f. zwei') {
        station = 'ORF2_Wien_HD'
    };
    if (station == 'a. t. v.') {
        station = 'ATV'
    };
    console.log("Zapping to: " + station);
    var options = {
        method: 'POST',
        url: 'https://android-api.horizon.tv/oesp/api/AT/deu/android/session',
        headers: {
            'cache-control': 'no-cache',
            'content-type': 'application/json'
        },
        body: {
            username: 'BADERTH',
            password: 'Asdf9276'
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var options = {
            method: 'POST',
            url: 'https://android-api.horizon.tv/oesp/api/AT/deu/android/settopboxes/tune',
            headers: {
                'cache-control': 'no-cache',
                'x-oesp-token': body.oespToken,
                'x-oesp-username': 'baderth',
                'content-type': 'application/json'
            },
            body: {
                serviceId: station,
                smartCardId: '1215507579'
            },
            json: true
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log(body);
        });

    });
}

function requestVerifier(req, res, next) {
    alexaVerifier(
        req.headers.signaturecertchainurl,
        req.headers.signature,
        req.rawBody,
        function verificationCallback(err) {
            if (err) {
                res.status(401).json({
                    message: 'Verification Failure',
                    error: err
                });
            } else {
                next();
            }
        }
    );
}

app.use(bodyParser.json({
    verify: function getRawBody(req, res, buf) {
        req.rawBody = buf.toString();
    }
}));

app.post('/forecast', requestVerifier, function (req, res) {
    console.log(req.body.request.type)
    console.log(req.body.request)
    if (req.body.request.type === 'LaunchRequest') { /* ... */ } else if (req.body.request.type === 'SessionEndedRequest') { /* ... */ } else if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'rimo') {

        if (!req.body.request.intent.slots.Day ||
            !req.body.request.intent.slots.Day.value) {
            // Handle this error by producing a response like:
            // "Hmm, what day do you want to know the forecast for?"
        }
        let day = new Date(req.body.request.intent.slots.Day.value);
        console.log(req.body.request.intent.slots.Day)
            // Do your business logic to get weather data here!
            // Then send a JSON response...

        res.json({
            "version": "1.0",
            "response": {
                "shouldEndSession": true,
                "outputSpeech": {
                    "type": "SSML",
                    "ssml": "<speak>Finger in Po, Mexiko! Paris - Athen - auf wiedersehn! " + day + "</speak>"
                }
            }
        });
    };

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'GetCurrentShows') {

        // Do your business logic to get weather data here!
        // Then send a JSON response...
        // https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-1&sort=startTime&byEndTime=
        request.get({
            url: ('https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-1&sort=startTime&byEndTime=' + moment().valueOf() + '~'),
            form: '{ "operation": "getSessionCount" }'
        }, function (error, response, body) {
            console.log(body);
            body = JSON.parse(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>" + body.listings[0].program.title + "</speak>"
                    }
                }
            });
        });


        // Then send a JSON response...

    }

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'GetNextShows') {

        // Do your business logic to get weather data here!
        // Then send a JSON response...
        // https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-1&sort=startTime&byEndTime=
        request.get({
            url: ('https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-2&sort=startTime&byEndTime=' + moment().valueOf() + '~'),
            form: '{ "operation": "getSessionCount" }'
        }, function (error, response, body) {
            console.log(body);
            body = JSON.parse(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>" + body.listings[1].program.title + "</speak>"
                    }
                }
            });
        });


        // Then send a JSON response...

    }


    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'ZapTo') {
        console.log("Station: " + req.body.request.intent.slots.Station.value);
        zapTo(req.body.request.intent.slots.Station.value);

        res.json({
            "version": "1.0",
            "response": {
                "shouldEndSession": true,
                "outputSpeech": {
                    "type": "SSML",
                    "ssml": "<speak>Ok, ich schalte um.</speak>"
                }
            }
        });

    }

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'GetPrimeTimeShows') {
        ts = moment().zone('Europe/Vienna').startOf('day').hour(20).minute(17).valueOf()
            // Do your business logic to get weather data here!
            // Then send a JSON response...
            // https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-1&sort=startTime&byEndTime=
        request.get({
            url: ('https://web-api-pepper.horizon.tv/oesp/v2/AT/deu/web/listings?byStationId=554823207416&range=1-2&sort=startTime&byEndTime=' + ts + '~'),
            form: '{ "operation": "getSessionCount" }'
        }, function (error, response, body) {
            console.log(body);
            body = JSON.parse(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>" + body.listings[0].program.title + ' gefolgt von ' + body.listings[1].program.title + "</speak>"
                    }
                }
            });
        });



        // Then send a JSON response...

    }




    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'sessions') {

        request.post({
            url: 'https://rimo-saas.com/api/MIT_Powerlines_SM/SDAlexaRESTController',
            form: '{ "operation": "getSessionCount" }'
        }, function (error, response, body) {
            console.log(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>Es sind " + body + " Sessions aktiv!</speak>"
                    }
                }
            });
        });

        // Then send a JSON response...

    }

    if (req.body.request.type === 'IntentRequest' &&
        req.body.request.intent.name === 'debug') {

        request.post({
            url: 'https://rimo-saas.com/api/MIT_Powerlines_SM/SDAlexaRESTController',
            form: '{ "operation": "getDebugCount" }'
        }, function (error, response, body) {
            console.log(body);
            res.json({
                "version": "1.0",
                "response": {
                    "shouldEndSession": true,
                    "outputSpeech": {
                        "type": "SSML",
                        "ssml": "<speak>Es gibt " + body + " Debugs!</speak>"
                    }
                }
            });
        });

        // Then send a JSON response...

    }
});
app.listen(3000);